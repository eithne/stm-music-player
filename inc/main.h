/*
 * main.h
 *
 *  Created on: Jan 21, 2014
 *      Author: eithne
 */

#ifndef MAIN_H_
#define MAIN_H_

#define FILE_BUFF_SIZE 8192
#define AUDIO_BUFFER_SIZE 4096

#define RED GPIO_Pin_14
#define GREEN GPIO_Pin_12
#define BLUE GPIO_Pin_15
#define ORANGE GPIO_Pin_13

#if defined  (HSE_VALUE)
/* Redefine the HSE value; it's equal to 8 MHz on the STM32F4-DISCOVERY Kit */
#undef HSE_VALUE
#define HSE_VALUE    ((uint32_t)8000000)
#else /* HSE_VALUE */
#define HSE_VALUE    ((uint32_t)8000000)
#endif

#include <stdint.h>
void NVIC_Initialize(void);
void DMA_Initialize(void);
void delay(uint32_t);
#endif /* MAIN_H_ */
