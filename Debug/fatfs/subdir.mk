################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../fatfs/diskio.c \
../fatfs/ff.c \
../fatfs/spi.c 

OBJS += \
./fatfs/diskio.o \
./fatfs/ff.o \
./fatfs/spi.o 

C_DEPS += \
./fatfs/diskio.d \
./fatfs/ff.d \
./fatfs/spi.d 


# Each subdirectory must supply rules for building sources it contributes
fatfs/%.o: ../fatfs/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -DSTM32F40_41xxx -DHSE_VALUE=8000000 -I"/home/eithne/new_workspace/CMSIS_STM32F4/inc" -I"/home/eithne/new_workspace/test-arm/helix" -I"/home/eithne/new_workspace/test-arm/fatfs" -I"/home/eithne/new_workspace/coocox-stm32f4/inc" -I"/home/eithne/new_workspace/test-arm/inc" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


