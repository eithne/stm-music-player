################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/codec.c \
../src/interrupts.c \
../src/main.c \
../src/semihosting.c \
../src/syscalls.c 

S_UPPER_SRCS += \
../src/sh_cmd.S \
../src/startup_stm32f40_41xxx.S 

OBJS += \
./src/codec.o \
./src/interrupts.o \
./src/main.o \
./src/semihosting.o \
./src/sh_cmd.o \
./src/startup_stm32f40_41xxx.o \
./src/syscalls.o 

C_DEPS += \
./src/codec.d \
./src/interrupts.d \
./src/main.d \
./src/semihosting.d \
./src/syscalls.d 

S_UPPER_DEPS += \
./src/sh_cmd.d \
./src/startup_stm32f40_41xxx.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -DSTM32F40_41xxx -DHSE_VALUE=8000000 -I"/home/eithne/new_workspace/CMSIS_STM32F4/inc" -I"/home/eithne/new_workspace/test-arm/helix" -I"/home/eithne/new_workspace/test-arm/fatfs" -I"/home/eithne/new_workspace/coocox-stm32f4/inc" -I"/home/eithne/new_workspace/test-arm/inc" -std=gnu99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -x assembler-with-cpp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


