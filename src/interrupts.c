/*
 * interrupts.c
 *
 *  Created on: Dec 13, 2013
 *      Author: eithne
 */

#include <stm32f4xx_spi.h>
#include <stm32f4xx.h>
#include <stm32f4xx_dma.h>
#include "codec.h"
#include "main.h"

extern volatile uint8_t readLeft;
extern volatile uint8_t readRight;

extern int16_t audioBuffer0[4096];
extern int16_t audioBuffer1[4096];

extern volatile uint32_t leftBufferSize;
extern volatile uint32_t rightBufferSize;

volatile uint32_t bufferNumber = 0;

void DMA1_Stream7_IRQHandler() {
	bufferNumber ^= 1;
	DMA1->HIFCR |= DMA_HIFCR_CTCIF7; // Clear interrupt flag.
	DMA1_Stream7->CR = (0 * DMA_SxCR_CHSEL_0) | // Channel 0
			(1 * DMA_SxCR_PL_0) | // Priority 1
			(1 * DMA_SxCR_PSIZE_0) | // PSIZE = 16 bit
			(1 * DMA_SxCR_MSIZE_0) | // MSIZE = 16 bit
			DMA_SxCR_MINC | // Increase memory address
			(1 * DMA_SxCR_DIR_0) | // Memory to peripheral
			DMA_SxCR_TCIE; // Transfer complete interrupt

	DMA1_Stream7->NDTR = bufferNumber ? rightBufferSize : leftBufferSize;

	DMA1_Stream7->PAR = (uint32_t) &SPI3->DR;
	DMA1_Stream7->M0AR =
			(uint32_t) (bufferNumber ? audioBuffer1 : audioBuffer0);
	DMA1_Stream7->FCR = DMA_SxFCR_DMDIS;
	DMA1_Stream7->CR |= DMA_SxCR_EN;
	if (bufferNumber)
		readLeft = 1;
	else
		readRight = 1;

}
