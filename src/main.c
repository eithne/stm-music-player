/* main.c
 *
 *  Created on: Dec 5, 2013
 *      Author: eithne
 */
#include "main.h"
#include <stdint.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_spi.h>
#include <stm32f4xx_i2c.h>
#include <stm32f4xx_dma.h>
#include <misc.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "codec.h"
#include "ff.h"
#include <pub/mp3dec.h>
#include "semihosting.h"

static uint32_t Mp3ReadId3V2Tag(FIL* pInFile, char* pszArtist,
		uint32_t unArtistSize, char* pszTitle, uint32_t unTitleSize);

void delay(uint32_t t) {
	while (t--) {

	}
}

void init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHB1PeriphClockCmd(
			RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC
					| RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1 | RCC_APB1Periph_SPI3, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	GPIO_InitTypeDef PinInitStruct;
	PinInitStruct.GPIO_Pin = GPIO_Pin_4;
	PinInitStruct.GPIO_Mode = GPIO_Mode_OUT;
	PinInitStruct.GPIO_OType = GPIO_OType_PP;
	PinInitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	PinInitStruct.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(GPIOD, &PinInitStruct);

	//PinInitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_9; //I2S SCL and SDA pins
	//GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_SPI3); //connecting pin 4 of port A to the SPI3 peripheral

	GPIO_SetBits(GPIOD, GPIO_Pin_4);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14
			| GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	//RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);//włączenie zegara GPIOE
	/* Konfiguracja pinu PA5 - SCK, SPI */
	GPIOA->MODER |= GPIO_MODER_MODER5_1;		//Alternate function
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5_1;	//50MHz Full Speed
	GPIOA->AFR[0] |= 0x00500000;				//AF5 = SCK

	/* Konfiguracja pinu PA6 - MISO, SPI */
	GPIOA->MODER |= GPIO_MODER_MODER6_1;		//Alternate function
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR6_1;	//50MHz Full Speed
	GPIOA->AFR[0] |= 0x05000000;				//AF5 = MISO

	/* Konfiguracja pinu PA7 - MOSI, SPI */
	GPIOA->MODER |= GPIO_MODER_MODER7_1;		//Alternate function
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR7_1;	//50MHz Full Speed
	GPIOA->AFR[0] |= 0x50000000;				//AF5 = MOSI

	/* Konfiguracja pinu PE3 - SPI/I2C select, 0=SPI, 1=I2C */
	GPIOE->MODER |= GPIO_MODER_MODER3_0;		//Output
	GPIOE->PUPDR |= GPIO_PUPDR_PUPDR3_1;		//Pull-down
	GPIO_ResetBits(GPIOE, GPIO_Pin_3);
	//GPIOE->BSRRH = 1 << 3;						//Output = 0 (SPI)

	SPI_InitTypeDef SPI_InitStruct;
	/* configure SPI2 in Mode 0
	 * CPOL = 0 --> clock is low when idle
	 * CPHA = 0 --> data is sampled at the first edge
	 */
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex; // set to full duplex mode, seperate MOSI and MISO lines
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master; // transmit in master mode, NSS pin has to be always high
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b; // one packet of data is 8 bits wide
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_High;        // clock is high when idle
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_2Edge;     // data sampled at second edge
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set; // set the NSS management to internal and pull internal NSS high
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16; // SPI frequency is APB2 frequency / 16
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB; // data is transmitted MSB first
	SPI_Init(SPI1, &SPI_InitStruct);

	SPI_Cmd(SPI1, ENABLE); // enable SPI1

}

char fileBuffer[FILE_BUFF_SIZE];
volatile uint8_t readLeft = 0;
volatile uint8_t readRight = 0;

FIL file;
DIR dir;

char files[100][12];
int32_t currentSong;
int32_t size;

void readFileList() {
	size = 0;
	currentSong = 0;

	FILINFO fno;
	do {
		f_readdir(&dir, &fno);
		if ((fno.fname[0] != '.') && fno.fname[0] != 0) {
			strcpy(files[size], fno.fname);
			size++;
		}
	} while (fno.fname[0] != 0);
}

void nextFile() {
	FRESULT res;
	do {
		f_close(&file);
		currentSong++;
		if (currentSong == size)
			currentSong = 0;
		res = f_open(&file, files[currentSong], FA_OPEN_EXISTING | FA_READ); // open existing file in read mode
	} while (res != FR_OK);
	char str[100];
	// Read ID3v2 Tag
	char szArtist[120];
	char szTitle[120];
	Mp3ReadId3V2Tag(&file, szArtist, sizeof(szArtist), szTitle,
			sizeof(szTitle));
	sprintf(str, "Artist=%s, Title=%s\n", szArtist, szTitle);
	SH_SendString(str);
}

void previousFile() {
	FRESULT res;
	do {
		f_close(&file);
		currentSong--;
		if (currentSong < 0)
			currentSong = size - 1;
		res = f_open(&file, files[currentSong], FA_OPEN_EXISTING | FA_READ); // open existing file in read mode
	} while (res != FR_OK);
	char str[100];
	// Read ID3v2 Tag
	char szArtist[120];
	char szTitle[120];
	Mp3ReadId3V2Tag(&file, szArtist, sizeof(szArtist), szTitle,
			sizeof(szTitle));
	sprintf(str, "Artist=%s, title=%s\n", szArtist, szTitle);
	SH_SendString(str);
}

void SPITransmitData(SPI_TypeDef* SPIx, uint8_t *sendBuff, uint8_t *recvBuff,
		int count) {

	//CS = 0
	GPIO_ResetBits(GPIOE, GPIO_Pin_3);

	while (count--) {

		while (!(SPIx->SR & SPI_SR_TXE))
			;	//Wait if TXE cleared, Tx FIFO is full.
		SPIx->DR = *(sendBuff++);
		while (!(SPIx->SR & SPI_SR_RXNE))
			;	//Wait if RNE cleared, Rx FIFO is empty.
		*(recvBuff++) = SPIx->DR;
	}

	//CS = 1
	GPIO_SetBits(GPIOE, GPIO_Pin_3);
}

//volatile int bytes_left;
//char *read_ptr;
HMP3Decoder hMP3Decoder;
MP3FrameInfo mp3FrameInfo;
int16_t audioBuffer0[AUDIO_BUFFER_SIZE];
int16_t audioBuffer1[AUDIO_BUFFER_SIZE];
volatile uint32_t leftBufferSize;
volatile uint32_t rightBufferSize;

void fillAudioBuffer(int buffer) {
	// buffer = 0 -> left ; buffer = 1 -> right
	int offset;
	int bufferOffset = 0;
	static int bytesLeft = FILE_BUFF_SIZE;
	int err;
	int16_t *samples;
	static char* inBuff = fileBuffer;

	if (buffer) {
		samples = audioBuffer1;
		rightBufferSize = 0;
	} else {
		samples = audioBuffer0;
		leftBufferSize = 0;
	}

	while (1) {

		offset = MP3FindSyncWord((unsigned char*) inBuff, bytesLeft);

		if (offset == -1) {
			break;
		}
		bytesLeft -= offset;
		inBuff += offset;

		err = MP3GetNextFrameInfo(hMP3Decoder, &mp3FrameInfo,
				(unsigned char*) inBuff);
		if (err != 0) {
			break;
		}

		if ((bufferOffset + mp3FrameInfo.outputSamps) > AUDIO_BUFFER_SIZE) {
			break;
		}
		err = MP3Decode(hMP3Decoder, (unsigned char**) &inBuff, &bytesLeft,
				samples, 0);
		if (err != 0) {
			break;
		}

		bufferOffset += mp3FrameInfo.outputSamps;
		samples+= mp3FrameInfo.outputSamps;
		if (buffer) {
			rightBufferSize += mp3FrameInfo.outputSamps;
		} else {
			leftBufferSize += mp3FrameInfo.outputSamps;
		}

		if (bytesLeft < (FILE_BUFF_SIZE / 2)) {
			unsigned int toRead;
			unsigned int read;
			memcpy(fileBuffer, inBuff, bytesLeft);
			inBuff = fileBuffer;
			toRead = FILE_BUFF_SIZE - bytesLeft;
			f_read(&file, fileBuffer + bytesLeft, toRead, &read);

			if (read != toRead) {
				GPIO_SetBits(GPIOD, GREEN);
				nextFile();
				f_read(&file, fileBuffer + bytesLeft + read,
						toRead - read, &read);
			}
			bytesLeft = FILE_BUFF_SIZE;
		}

	}
}

int main(void) {
	SystemInit();
	SystemCoreClockUpdate();
	init();
	codec_init();
	codec_ctrl_init();

	hMP3Decoder = MP3InitDecoder();

	uint8_t i = 6;
	while (i--) {
		GPIO_SetBits(GPIOD, GPIO_Pin_12);
		delay(0x3FFFF);
		GPIO_ResetBits(GPIOD, GPIO_Pin_12);
		delay(0x3FFFF);
		GPIO_SetBits(GPIOD, GPIO_Pin_13);
		delay(0x3FFFF);
		GPIO_ResetBits(GPIOD, GPIO_Pin_13);
		delay(0x3FFFF);
		GPIO_SetBits(GPIOD, GPIO_Pin_14);
		delay(0x3FFFF);
		GPIO_ResetBits(GPIOD, GPIO_Pin_14);
		delay(0x3FFFF);
		GPIO_SetBits(GPIOD, GPIO_Pin_15);
		delay(0x3FFFF);
		GPIO_ResetBits(GPIOD, GPIO_Pin_15);
		delay(0x3FFFF);
	}

	NVIC_Initialize();
	I2S_Cmd(CODEC_I2S, ENABLE);

//-------------------------------------------
	FATFS FatFs;
	FRESULT res;
	res = f_mount(&FatFs, "", 1); // mount the drive
	if (res) {
		while (1)
			;
	}
	f_opendir(&dir, "/");
	readFileList();
	nextFile();
	UINT read;

	f_read(&file, (void*) fileBuffer, FILE_BUFF_SIZE, &read);

	readLeft = 0;
	readRight = 0;
	fillAudioBuffer(0);
	fillAudioBuffer(1);

	DMA_Initialize();

	GPIO_SetBits(GPIOE, GPIO_Pin_3);
	uint8_t txbuff[10];
	uint8_t rxbuff[10];

//first byte struct:
//R/~W   AutoIncr   RRRRRR
#define CTRL_REG1 0x20
#define CTRL_REG2 0x21
#define STATUS_REG 0x27
#define ZYXDA 3
#define OUT_X 0x29

//power on
	txbuff[0] = 0b00000000 | CTRL_REG1;
	txbuff[1] = 0b01000111;
	SPITransmitData(SPI1, txbuff, rxbuff, 2);

	uint32_t previous = 0;
	uint32_t next = 0;
	uint8_t blockNext = 0;
	uint8_t blockPrev = 0;

	while (1) {
		if (readLeft) {
			readLeft = 0;
			fillAudioBuffer(0);
			GPIO_SetBits(GPIOD, RED);

		} else if (readRight) {
			readRight = 0;
			fillAudioBuffer(1);
			GPIO_ResetBits(GPIOD, RED);
		}

		//accelerometer
		txbuff[0] = 0b10000000 | STATUS_REG;
		txbuff[1] = 0xff;
		SPITransmitData(SPI1, txbuff, rxbuff, 2);

		if (rxbuff[1] & (1 << ZYXDA)) {

			txbuff[0] = 0b11000000 | OUT_X;
			txbuff[1] = 0xff;
			SPITransmitData(SPI1, txbuff, rxbuff, 4);
			if ((int8_t) rxbuff[1] > 30) {
				if (!blockNext) {
					GPIO_SetBits(GPIOD, GPIO_Pin_15);
					previous++;
					if (previous == 10000) {
						previousFile();
						previous = 0;
						blockNext = 1;
					}
				}
			} else {
				previous = blockNext = 0;
				GPIO_ResetBits(GPIOD, GPIO_Pin_15);
			}

			if ((int8_t) rxbuff[1] < -30) {
				if (!blockPrev) {
					GPIO_SetBits(GPIOD, GPIO_Pin_13);
					next++;
					if (next >= 10000) {
						nextFile();
						next = 0;
						blockPrev = 1;
					}
				}
			} else {
				next = blockPrev = 0;
				GPIO_ResetBits(GPIOD, GPIO_Pin_13);
			}
		}

	}

	return 0;
}

void DMA_Initialize(void) {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);

	DMA_InitTypeDef dma_init;

	dma_init.DMA_Channel = DMA_Channel_0;
	dma_init.DMA_Memory0BaseAddr = (uint32_t) audioBuffer0;
	dma_init.DMA_PeripheralBaseAddr = (uint32_t) &SPI3->DR;	// (uint32_t)(0x4000380C); //SPI2 DR
	dma_init.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	dma_init.DMA_PeripheralDataSize = DMA_MemoryDataSize_HalfWord;
	dma_init.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	dma_init.DMA_Mode = DMA_Mode_Normal;
	dma_init.DMA_MemoryInc = DMA_MemoryInc_Enable;
	dma_init.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	dma_init.DMA_BufferSize = leftBufferSize;
	dma_init.DMA_Priority = DMA_Priority_High;
	dma_init.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	dma_init.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	dma_init.DMA_FIFOMode = DMA_FIFOMode_Disable;

	DMA_Init(DMA1_Stream7, &dma_init);
	DMA_Cmd(DMA1_Stream7, ENABLE);

	NVIC_EnableIRQ(DMA1_Stream7_IRQn);

	DMA_ITConfig(DMA1_Stream7, DMA_IT_TC, ENABLE);
	SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, ENABLE);
}

void NVIC_Initialize(void) {
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	NVIC_InitStructure.NVIC_IRQChannel = SPI3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

static uint32_t Mp3ReadId3V2Text(FIL* pInFile, uint32_t unDataLen,
		char* pszBuffer, uint32_t unBufferSize) {
	UINT unRead = 0;
	BYTE byEncoding = 0;
	if ((f_read(pInFile, &byEncoding, 1, &unRead) == FR_OK) && (unRead == 1)) {
		unDataLen--;
		if (unDataLen <= (unBufferSize - 1)) {
			if ((f_read(pInFile, pszBuffer, unDataLen, &unRead) == FR_OK)
					|| (unRead == unDataLen)) {
				if (byEncoding == 0) {
					// ISO-8859-1 multibyte
					// just add a terminating zero
					pszBuffer[unDataLen] = 0;
				} else if (byEncoding == 1) {
					// UTF16LE unicode
					uint32_t r = 0;
					uint32_t w = 0;
					if ((unDataLen > 2) && (pszBuffer[0] == 0xFF)
							&& (pszBuffer[1] == 0xFE)) {
						// ignore BOM, assume LE
						r = 2;
					}
					for (; r < unDataLen; r += 2, w += 1) {
						// should be acceptable for 7 bit ascii
						pszBuffer[w] = pszBuffer[r];
					}
					pszBuffer[w] = 0;
				}
			} else {
				return 1;
			}
		} else {
			// we won't read a partial text
			if (f_lseek(pInFile, f_tell(pInFile) + unDataLen) != FR_OK) {
				return 1;
			}
		}
	} else {
		return 1;
	}
	return 0;
}

/*
 * Taken from
 * http://www.mikrocontroller.net/topic/252319
 */
static uint32_t Mp3ReadId3V2Tag(FIL* pInFile, char* pszArtist,
		uint32_t unArtistSize, char* pszTitle, uint32_t unTitleSize) {
	pszArtist[0] = 0;
	pszTitle[0] = 0;

	BYTE id3hd[10];
	UINT unRead = 0;
	if ((f_read(pInFile, id3hd, 10, &unRead) != FR_OK) || (unRead != 10)) {
		return 1;
	} else {
		uint32_t unSkip = 0;
		if ((unRead == 10) && (id3hd[0] == 'I') && (id3hd[1] == 'D')
				&& (id3hd[2] == '3')) {
			unSkip += 10;
			unSkip = ((id3hd[6] & 0x7f) << 21) | ((id3hd[7] & 0x7f) << 14)
					| ((id3hd[8] & 0x7f) << 7) | (id3hd[9] & 0x7f);

			// try to get some information from the tag
			// skip the extended header, if present
			uint8_t unVersion = id3hd[3];
			if (id3hd[5] & 0x40) {
				BYTE exhd[4];
				f_read(pInFile, exhd, 4, &unRead);
				size_t unExHdrSkip = ((exhd[0] & 0x7f) << 21)
						| ((exhd[1] & 0x7f) << 14) | ((exhd[2] & 0x7f) << 7)
						| (exhd[3] & 0x7f);
				unExHdrSkip -= 4;
				if (f_lseek(pInFile, f_tell(pInFile) + unExHdrSkip) != FR_OK) {
					return 1;
				}
			}
			uint32_t nFramesToRead = 2;
			while (nFramesToRead > 0) {
				char frhd[10];
				if ((f_read(pInFile, frhd, 10, &unRead) != FR_OK)
						|| (unRead != 10)) {
					return 1;
				}
				if ((frhd[0] == 0) || (strncmp(frhd, "3DI", 3) == 0)) {
					break;
				}
				char szFrameId[5] = { 0, 0, 0, 0, 0 };
				memcpy(szFrameId, frhd, 4);
				uint32_t unFrameSize = 0;
				uint32_t i = 0;
				for (; i < 4; i++) {
					if (unVersion == 3) {
						// ID3v2.3
						unFrameSize <<= 8;
						unFrameSize += frhd[i + 4];
					}
					if (unVersion == 4) {
						// ID3v2.4
						unFrameSize <<= 7;
						unFrameSize += frhd[i + 4] & 0x7F;
					}
				}

				if (strcmp(szFrameId, "TPE1") == 0) {
					// artist
					if (Mp3ReadId3V2Text(pInFile, unFrameSize, pszArtist,
							unArtistSize) != 0) {
						break;
					}
					nFramesToRead--;
				} else if (strcmp(szFrameId, "TIT2") == 0) {
					// title
					if (Mp3ReadId3V2Text(pInFile, unFrameSize, pszTitle,
							unTitleSize) != 0) {
						break;
					}
					nFramesToRead--;
				} else {
					if (f_lseek(pInFile, f_tell(pInFile) + unFrameSize)
							!= FR_OK) {
						return 1;
					}
				}
			}
		}
		if (f_lseek(pInFile, unSkip) != FR_OK) {
			return 1;
		}
	}

	return 0;
}

